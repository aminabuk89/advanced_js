"use strict";
// ## Завдання
// - Створити сторінку, яка імітує стрічку новин соціальної мережі [Twitter](https://twitter.com/).

// #### Технічні вимоги:

//  - При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. Для цього потрібно надіслати GET запит на наступні дві адреси:
//    - `https://ajax.test-danit.com/api/json/users`
//    - `https://ajax.test-danit.com/api/json/posts`
//  - Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
//  - Кожна публікація має бути відображена у вигляді картки (приклад в папці), та включати заголовок, текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
//  - На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки. При натисканні на неї необхідно надіслати DELETE запит на адресу `https://ajax.test-danit.com/api/json/posts/${postId}`. Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
//  - Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти [тут](https://ajax.test-danit.com/api-pages/jsonplaceholder.html).
//  - Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. Це нормально, все так і має працювати.
//  - Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас `Card`. При необхідності ви можете додавати також інші класи.

// #### Необов'язкове завдання підвищеної складності
//  - Поки з сервера під час відкриття сторінки завантажується інформація, показувати анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
//  - Додати зверху сторінки кнопку `Додати публікацію`. При натисканні на кнопку відкривати модальне вікно, в якому користувач зможе ввести заголовок та текст публікації. Після створення публікації дані про неї необхідно надіслати в POST запиті на адресу:  `https://ajax.test-danit.com/api/json/posts`. Нова публікація має бути додана зверху сторінки (сортування у зворотному хронологічному порядку). Автором можна присвоїти публікації користувача з `id: 1`.
//  - Додати функціонал (іконку) для редагування вмісту картки. Після редагування картки для підтвердження змін необхідно надіслати PUT запит на адресу `https://ajax.test-danit.com/api/json/posts/${postId}`.

const URL_USERS = `https://ajax.test-danit.com/api/json/users`;
const URL_POST = `https://ajax.test-danit.com/api/json/posts`;
const root = document.querySelector(`#root`);
class Card {
  constructor(postData, userData) {
    this.postData = postData;
    this.userData = userData;
  }
  createCard() {
    const temp = document.querySelector("#create-post");
    const div = temp.content.querySelector(".div-post").cloneNode(true); 
    const title = div.querySelector("h2");
    title.textContent = this.postData.title;
    const img = div.querySelector("img");
    img.src = "com.svg";
    const divText = div.querySelector(".div-text");
    const text = div.querySelector(".text");
    text.textContent = this.postData.body;
    const name = div.querySelector(".name");
    name.textContent = this.userData.name;
    const email = div.querySelector(".email");
    email.textContent = this.userData.email;
    const deleteButton = div.querySelector("button");
    deleteButton.textContent = "Delete";
    deleteButton.addEventListener("click", () => {
      this.deletePost();
    });
    return div;
  }
  deletePost() {
   return fetch(`${URL_POST}/${this.postData.id}`, {
      method: "DELETE"
    })
    .then(response => {
      if (!response.ok) {
        throw new Error("Error deleting post: Server response was not ok");
      }
      return response.json();
    })
    .then(data => {
      console.log("Post deleted successfully:", data);
      const cardElement = document.getElementById(`post-${this.postData.id}`);
      if (cardElement) {
        cardElement.remove();
      }
    })
    .catch(error => {
      console.error("Error deleting post:", error);
    });

  }
}
function fetchUsers() {
  return fetch(`${URL_USERS}`)
    .then(response => response.json())
    .catch(error => console.error("Error fetching users data:", error));
}
function fetchPosts() {
  return fetch(`${URL_POST}`)
    .then(response => response.json())
    .catch(error => console.error("Error fetching posts data:", error));
}
function renderCards(users, posts) {
  const root = document.getElementById("root");
  posts.forEach(post => {
    const userData = users.find(user => user.id === post.userId);
    if (userData) {
      const card = new Card(post, userData);
      const cardElement = card.createCard();
      cardElement.id = `post-${post.id}`;
      root.append(cardElement);
    }
  });
}
Promise.all([fetchUsers(), fetchPosts()])
  .then(([users, posts]) => {
    renderCards(users, posts);
    console.log(users);
    console.log(posts);
  })
  .catch(error => {
    console.error("Error:", error);
  });

