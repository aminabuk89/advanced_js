`use strict`;
//                      ## Теоретичне питання
// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію `try...catch`.

// `try...catch` доречно використвувати, коли ми хочемо обробляти помилку і продовжити виконання.
// Перший приклад: рішення до практичного завдання.
// Використовуємо try{}, в якому пишемо if(який перевіряє навність всіх трьох властивостей){} і else{який викидає помилку, якщо якась властивості дорівнює undefined, JSON.stringify - створює новий об'єкт помилки, яке містить рядок JSON}. Далі прописуємо catch (e) {}, який обробляє нашу помилку (якщо вона є).

// Другий приклад: try{виводить результат додавання a і b}, catch (e) {працює, коли  в першому блоці помилка.}. Так як оголошення змінних  a і b не було, тому працює catch.
// try{
// let result = a + b;
// console.log(result);
// }  catch(e){
//     console.error(e.name, e.message);
// }

// Третій приклад: в try{викликаємо фу add, якій передаємо параметром числа}, catch (e) {тут прописуємо помилку}. Так як фу add не оголошена, її викликати не можливо, тому працює блок catch.
// try{
// console.log(add(4, 6));
// }  catch(e){
//     console.error(e.name, e.message);
// }
//                       ## Завдання
// Дано масив books.
// ```javascript
// ```
// - Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// - На сторінці повинен знаходитись `div` з `id="root"`, куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
// - Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
// - Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.
// #### Примітка
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
// #### Література:
// - [Перехоплення помилок, "try..catch"](https://learn.javascript.ru/exception)
// - [try...catch на MDN](https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Statements/try...catch)
const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

function validate(book) {
  const requiredProperties = ['author', 'name', 'price'];
  for (const prop of requiredProperties) {
      if (!Object.keys(book).includes(prop)) {
          throw new Error(`Книга "${book.name}" не містить властивості "${prop}"`);
      }
  }
}

function render(books) {
  const root = document.createElement("div");
  root.id = "root";
  const ul = document.createElement("ul");
  ul.className = "books";
  root.append(ul);
  const fram = document.createDocumentFragment();
  books.forEach((book) => {
    try {
      validate(book);
      const li = document.createElement("li");
      for (const prop in book) {
          const p = document.createElement("p");
          p.textContent = `${prop}: ${book[prop]}`;
          li.appendChild(p);
      }
      fram.append(li);
    } catch (e) {
      console.error(e.message);
    }
  });
  ul.append(fram);
  document.body.prepend(root);
}
render(books);