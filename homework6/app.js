`use strict`;
// ## Теоретичне питання
// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript
// Асинхронності у Javascript - це те, що не виконується крок за кроком (послідовно), наприклад: як написан html в такій послідовності він і буде виконуватись; операції не чекають, поки інші завершаться, а продовжують своє виконання паралельно.

// ## Завдання
// Написати програму "Я тебе знайду по IP"
// #### Технічні вимоги:
// - Створити просту HTML-сторінку з кнопкою `Знайти по IP`.
// - Натиснувши кнопку - надіслати AJAX запит за адресою `https://api.ipify.org/?format=json`, отримати звідти IP адресу клієнта.
// - Дізнавшись IP адресу, надіслати запит на сервіс `https://ip-api.com/` та отримати інформацію про фізичну адресу.
// - під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// - Усі запити на сервер необхідно виконати за допомогою async await.
// ## Примітка
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
// #### Литература:
// - [Async/await](https://learn.javascript.ru/async-await)
// - [async function](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Statements/async_function)
// - [Документація сервісу ip-api.com](http://ip-api.com/docs/api:json)

let button = document.querySelector(".btn");
button.addEventListener("click", async function onClickBtn() {
  try {
    const responseIpify = await fetch(`https://api.ipify.org/?format=json`);
    const dataIpify = await responseIpify.json();
    const ipAddress = dataIpify.ip;
    console.log(dataIpify);

    const responseIpApi = await fetch(
      `http://ip-api.com/json/${ipAddress}?fields=country,regionName,city,continent,district`
    );
    console.log(responseIpApi);
    const dataIpApi = await responseIpApi.json();
    console.log(dataIpApi);
    const info = document.querySelector(".info");
    info.innerHTML = `
        <p>Країна: ${dataIpApi.country}</p>
        <p>Регіон: ${dataIpApi.regionName}</p>
        <p>Місто: ${dataIpApi.city}</p>
        <p>Континент: ${dataIpApi.continent}</p>
        <p>Район: ${dataIpApi.district}</p>`;
  } catch (error) {
    console.error("Помилка:", error);
  }
});
