"use strict";
// ##               Теоретичне питання

// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
// AJAX - технологія взаємодії з сервером. Дозволяє надсилати та отримувати дані від веб-сервера без перевантаження сторінки. 

// ##               Завдання

// Отримати список фільмів серії `Зоряні війни` та вивести на екран список персонажів для кожного з них.
// #### Технічні вимоги:
// - Надіслати AJAX запит на адресу `https://ajax.test-danit.com/api/swapi/films` та отримати список усіх фільмів серії `Зоряні війни`
// - Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості `characters`.
// - Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля `episodeId`, `name`, `openingCrawl`).
// - Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

// #### Необов'язкове завдання підвищеної складності
//  - Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
// ## Примітка
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Література:
// - [Використання Fetch на MDN](https://developer.mozilla.org/ru/docs/Web/API/Fetch_API/Using_Fetch)
// - [Fetch](https://learn.javascript.ru/fetch)
// - [CSS анімація](https://html5book.ru/css3-animation/)
// - [Події DOM](https://learn.javascript.ru/introduction-browser-events)

const BASE_URL = "https://ajax.test-danit.com/api/swapi/films";

const div = document.createElement("div");
document.body.append(div);

fetch(BASE_URL)
  .then((response) => response.json())
  .then((data) => {
    console.log(data);
    const ul = document.createElement("ul");
    div.appendChild(ul);

    data.forEach((item) => {
      console.log(item);
      const li = document.createElement("li");
      li.innerHTML = `EpisodeId: ${item.episodeId},<br>Name: ${item.name},<br>OpeningCrawl: ${item.openingCrawl}<br>`;
      const charactersPromises = item.characters.map((characterUrl) =>
        fetch(characterUrl).then((response) => response.json())
      );
      Promise.all(charactersPromises)
        .then((charactersData) => {
          const charactersList = document.createElement("ul");
          charactersData.forEach((character) => {
            const characterLi = document.createElement("li");
            characterLi.innerHTML = character.name;
            charactersList.appendChild(characterLi);
          });
          li.appendChild(charactersList);
        })
        .catch((e) => {
          console.error(e);
        });

      ul.appendChild(li);
    });
  })
  .catch((e) => {
    console.error(e);
  });

