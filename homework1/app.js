"use strict"
// <!-- ##                     Теоретичне питання
// 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Об'єкти можуть успадковувати властивості один від одного за допомогою протоnипа. Прототип - це такж об'єкт, в якого є свій прототип. Це називається прототипним наслідуванням. І воно закінчується тоді, коли в останнього об'єкта немає прототипа, а значення - null.

// 2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
// Для того, щоб клас-нащадок отримав ті самі властивості та методи, що і у батька.

//                              ## Завдання
// 1. Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// 2. Створіть гетери та сеттери для цих властивостей.
// 3. Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// 4. Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// 5. Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

// ## Примітка
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Література:
// - [Класи на MDN](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Classes)
// - [Класи в ECMAScript 6](https://frontender.info/es6-classes-final/) -->

class Employee {
    constructor(name, age, salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name(){
        return this._name
    }
    get age(){
        return this._age
    }
    get salary(){
        return this._salary
    }
    set name(newName){
        return  this._name = newName;
    }
    set age(newAge){
        return  this._age = newAge;
    }
    set salary(newSalary){
        return  this._salary = newSalary;
    }
}
class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary)
    this.lang = lang;
    }
    get salary() {
        return super.salary * 3;
    }
}
let person1 = new Employee("Alisa", 34, 1000);
let person2 = new Employee("Den", 20, 500);

let person3 = new Programmer("Vova", 35, 1500, ["Java,", "Python,", "PHP"])
let person4 = new Programmer("Vika", 43, 876, ["JavaScript,", "C++"])
let person5 = new Programmer("Kate", 54, 2340, "Ruby")

console.log(person1);
console.log(person2);
console.log("Programmer 1:", person3.name, person3.age +" years old", person3.salary + "$,", "programming languages:", ...person3.lang);
console.log("Programmer 2:", person4.name, person4.age +" years old", person4.salary + "$,", "programming languages:", ...person4.lang);
console.log("Programmer 3:", person5.name, person5.age +" years old", person5.salary + "$,", "programming languages:", person5.lang);
